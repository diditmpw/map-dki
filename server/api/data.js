export default defineEventHandler(async (event) => {
    const { area } = getQuery(event);
    //data diambil dari data.jakarta.go.id
    const json1 = [

        {
            "tahun": 2012,
            "kab/kota": "Kepulauan Seribu",
            "luas_area": 8.7,
            "jumlah_penduduk": 22220,
            "kepadatan_penduduk": 2554.02
        },
        {
            "tahun": 2012,
            "kab/kota": "Jakarta Selatan",
            "luas_area": 141.27,
            "jumlah_penduduk": 2148261,
            "kepadatan_penduduk": 15206.77
        },
        {
            "tahun": 2012,
            "kab/kota": "Jakarta Timur",
            "luas_area": 188.03,
            "jumlah_penduduk": 2801784,
            "kepadatan_penduduk": 14900.73
        },
        {
            "tahun": 2012,
            "kab/kota": "Jakarta Pusat",
            "luas_area": 48.13,
            "jumlah_penduduk": 908829,
            "kepadatan_penduduk": 18882.8
        },
        {
            "tahun": 2012,
            "kab/kota": "Jakarta Barat",
            "luas_area": 129.54,
            "jumlah_penduduk": 2395130,
            "kepadatan_penduduk": 18489.5
        },
        {
            "tahun": 2012,
            "kab/kota": "Jakarta Utara",
            "luas_area": 146.66,
            "jumlah_penduduk": 1715564,
            "kepadatan_penduduk": 11697.56
        }
    ];
    const json2 = [

        {
            "tahun": 2020,
            "wilayah": "Kepulauan Seribu",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 43
        },
        {
            "tahun": 2020,
            "wilayah": "Kepulauan Seribu",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 89
        },
        {
            "tahun": 2020,
            "wilayah": "Kepulauan Seribu",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 57
        },
        {
            "tahun": 2020,
            "wilayah": "Kepulauan Seribu",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 15
        },
        {
            "tahun": 2020,
            "wilayah": "Kepulauan Seribu",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 9
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Selatan",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 2338
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Selatan",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 7764
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Selatan",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 1659
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Selatan",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 1993
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Selatan",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 296
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Timur",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 1861
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Timur",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 8030
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Timur",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 1840
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Timur",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 1709
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Timur",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 338
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Pusat",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 2530
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Pusat",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 9824
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Pusat",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 1227
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Pusat",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 1772
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Pusat",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 366
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Barat",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 1995
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Barat",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 6167
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Barat",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 1281
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Barat",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 1536
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Barat",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 279
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Utara",
            "tenaga_kesehatan": "Dokter",
            "jumlah": 1345
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Utara",
            "tenaga_kesehatan": "Perawat",
            "jumlah": 4341
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Utara",
            "tenaga_kesehatan": "Bidan",
            "jumlah": 1063
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Utara",
            "tenaga_kesehatan": "Farmasi",
            "jumlah": 1102
        },
        {
            "tahun": 2020,
            "wilayah": "Jakarta Utara",
            "tenaga_kesehatan": "Ahli Gizi",
            "jumlah": 229
        }

    ]

    const data = area?{ area, ...json1.find(val => val["kab/kota"] === area), petugas: [...json2.filter(val => val["wilayah"] === area)] }:{...json1.map(val => {return{...val, area: val["kab/kota"]}} )};

    return data;

})